module.exports = {
  collectCoverage: true,
  setupFiles: [
    "<rootDir>/config/setup.js"
  ]
};