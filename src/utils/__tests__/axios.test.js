import axios from 'axios';

jest.mock('axios');

describe('axios test', () => {
  it('should mock axios', () => {
    const users = [{name: 'Bob'}];
    const resp = {data: users};
    // 指定axios.get请求的返回值
    axios.get.mockResolvedValue(resp);
    // 获取数据
    axios.get('/axios/get/test')
      .then(response => {
        const receive = response.data;
        expect(receive).toEqual(resp);
      })
      .catch(error => new Error(error));
  });
});