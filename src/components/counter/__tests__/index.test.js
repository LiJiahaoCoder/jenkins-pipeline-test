import React from 'react';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import configStore from 'redux-mock-store';

import ConnectedCounter, {Counter} from '../index';

// mock store
const mockStore = configStore([thunk]);
const store = mockStore({count: 0});
// mock action
const mockAddOne = number => {
  return ({
    type: 'ADD_ONE',
    payload: ++number
  });
}

describe('Counter component test', () => {
  it('should call handle click event', () => {
    // 1. 监视handleClick方法
    const spy = sinon.spy(Counter.prototype, 'handleClick');
    const component = mount(<Counter />);
    // 2. 模拟用户点击按钮
    component.find('.plus').at(0).simulate('click');
    // 3. 获取点击button后的state
    const firstResult = component.state('count');
    expect(firstResult).toBe(1); // 点击一次+之后，结果应该为1
    // 4. 获取handleClick调用的次数
    expect(spy.calledOnce).toBeTruthy(); // handleClick被调用了一次
  });
  it('should get connected component', () => {
    // 1. 监视handleClick方法
    const spy = sinon.spy(Counter.prototype, 'handleClick');
    const component = mount(
      <Provider store={store}>
        <ConnectedCounter />
      </Provider>
    );
    // 2. 模拟用户点击按钮
    component.find('.plus').simulate('click');
    expect(spy.calledOnce).toBeTruthy(); // handleClick被调用了一次
    // 3. 获取dispatch action后store中的值
    const actions = store.getActions();
    const expectResult = {
      type: 'ADD_ONE',
      payload: 1
    };
    expect(actions[0]).toEqual(expectResult);
  });
});