function addOne(number) {
  number++;
  return ({
    type: 'ADD_ONE',
    payload: number
  });
}

function minusOne(number) {
  number--;
  return ({
    type: 'MINUS_ONE',
    payload: number
  });
}

export {
  addOne,
  minusOne
}