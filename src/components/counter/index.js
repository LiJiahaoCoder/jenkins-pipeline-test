import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addOne, minusOne } from './action';

class Counter extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   count: 0
    // };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(operator) {
    const { count } = this.props;
    // let { count } = this.state;
    // switch (operator) {
    //   case '+':
    //     count++;
    //     break;
    //   case '-':
    //     count--;
    //     break;
    //   default:
    //     break;
    // }
    // this.setState({count});
    switch (operator) {
      case '+':
        this.props.addOne(count);
        break;
      case '-':
        this.props.minusOne(count);
        break;
      default:
        break;
    }
  }

  render() {
    const count = this.props.count || 0;
    return (
      <div>
        <button className='plus' onClick={() => this.handleClick('+')}> + </button>
        点击了: {count} 次
        <button className='minus' onClick={() => this.handleClick('-')}> - </button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  count: state.count
});

const mapDispatchToProps = dispatch => ({
  addOne: number => dispatch(addOne(number)),
  minusOne: number => dispatch(minusOne(number))
});

export { Counter };
export default connect(mapStateToProps, mapDispatchToProps)(Counter);