const initState = {
  count: 0
};

export default countReducer = (state = initState, action) => {
  switch (action.type) {
    case 'ADD_ONE':
      return {
        ...state,
        count: action.payload
      }
    case 'MINUS_ONE':
      return {
        ...state,
        count: action.payload
      }
    default:
      return state;
  }
}