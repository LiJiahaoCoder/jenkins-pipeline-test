import React, { Component } from 'react';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date().toLocaleDateString()
    };
  }

  render() {
    const { navs } = this.props;
    const { date } = this.state;
    return (
      <div>
        <ul>
          {
            navs.map(nav => <li key={nav.link}>
                <a href={nav.link}>{nav.text}</a>
              </li>
            )
          }
        </ul>
        <h1>Welcome to my home!</h1>
        <h2>Today is {date}</h2>
      </div>
    )
  }
}
