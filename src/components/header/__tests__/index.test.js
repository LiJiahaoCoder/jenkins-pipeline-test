import React from 'react';
import { mount } from 'enzyme';

import Header from '../index';

const NAVS = [
  {
    text: '首页',
    link: '/home'
  },
  {
    text: '个人信息',
    link: '/profile'
  }
];

describe('Header component test', () => {
  it('should get h1 content', () => {
    const component = mount(<Header />);
    expect(component.find('h1').text()).toBe('Welcome to my home!');
  });
  it('should get date in state', () => {
    const component = mount(<Header />);
    // expect(component.state().date).toBe(new Date().toLocaleDateString());
    expect(component.state('date')).toBe(new Date().toLocaleDateString());
    expect(component.find('h2').text()).toBe(`Today is ${new Date().toLocaleDateString()}`);
  });
  it('should get correct props', () => {
    const component = mount(<Header navs={NAVS} />);
    // expect(component.props().navs).toBe(NAVS);
    expect(component.prop('navs')).toBe(NAVS);
    expect(component.find('a').length).toBe(2);
  });
});